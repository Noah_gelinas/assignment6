"use strict";


const {useState, useEffect} = React;

function LeftSideView({types,pokemons,getPokemon,getDetails,selected}) {
    return (
    <header>
        {
            <h3>Select a type</h3>
        }
    
        <select onChange={(e) => {getPokemon(e.target.value)
                              }}>
            {
                types.map(type=> <option>{type.name}</option>)
            }
        </select>

        {
            <ul onClick={(e) => {
                getDetails(e.target.textContent);
                selected();
                e.target.className="selected"
                }}>
                {
                    pokemons.map(pokemon => <li className={"bold"}>{pokemon.pokemon.name}</li>)
                }
            </ul>
        }

    </header>
    );
}

function RightSideView({selectedPokemon,move,ability,unHideText,getRegion,region,recentlyClicked,showMsg}) {
    if(!selectedPokemon){
        return(
            <main>
                <p >
                    Please select a type and pokemon to find information for!
                </p>
            </main>
        )
    }
    return (
        <main>
            {
            <div className="artist-info">
                <h1>{selectedPokemon.name}</h1>
                    <img src={selectedPokemon.front} alt={selectedPokemon.name}/>
                    <img src={selectedPokemon.front_shiny}/>
                    <img src={selectedPokemon.back}/>
                    <img src={selectedPokemon.back_shiny}/>
                      
                
                    <nav onClick={(e)=>unHideText(e.target.className)}>
                        <button className={"Ability"}>Ability</button>
                        <button className={"Move"}>Move</button>
                        <button className={"Stats"}>Stats</button>
                        <button className={"Game"}>What games {selectedPokemon.name} appear in</button>
                    </nav>
                    <div>
                        <h4 className={"Stats hide"}>{selectedPokemon.name} Base Stats</h4>
                        <h4 className={"Ability hide"}>{selectedPokemon.abilityName}</h4>
                                
                        <h4 className={"Move hide"}>{selectedPokemon.moveName}</h4>

                        <h4 className={"Game hide"}>Games {selectedPokemon.name} can be found in: <br/> 
                        Click to find Region!</h4>
                
                        <ul className={"Stats hide"}>
                            {selectedPokemon.stats.map((e)=>(
                            <li>
                                {e.stat.name}: {e.base_stat}
                            </li>
                        ))}
                        </ul>

                        <ul className={"Game hide"} onClick={
                            (e)=>{
                                getRegion(e.target.textContent)
                                showMsg();
                                }
                            }>
                            {selectedPokemon.games.map((e)=>(
                                <li>
                                    {e.version.name}
                                </li>
                            ))}
                        </ul>

                        <p className={"Ability hide"}>{ability.ability}</p>
                
                        <p className={"Move hide"}>{move.move}</p>

                        <h3 className={"hide"}>Pokemon {recentlyClicked.name} is based in the {region.name} region</h3>
                    </div>

                    
            </div>
            }
        </main>
    )
}

function App() {
    const [types, setTypes] = useState([]);
    const [pokemons, setPokemons] = useState([]);
    const [selectedPokemon, setSelectedPokemon] = useState(null);
    const [ability,setAbility]=useState("");
    const [move,setMove]=useState("");
    const [region,setRegion]=useState("");
    const [recentlyClicked,setRecentlyClicked]=useState("");
    
    function getPokemon(type) {
        fetch(`https://pokeapi.co/api/v2/type/${type}`, {})
        .then(response => {
            if(!response.ok) {
                throw new Error("Not 2xx response", {cause: response})
            }
              return response.json()
            })
            .then(obj => {
                setPokemons(obj.pokemon);
            })
    }

    function getDetails(name) {
        fetch(`https://pokeapi.co/api/v2/pokemon/${name}`,{})
        .then(response => {
            if(!response.ok) {
                throw new Error("Not 2xx response", {cause: response})
            }
              return response.json()
            })
            .then(obj => {
                setSelectedPokemon({
                    name:obj.name,
                    front: obj.sprites.front_default,
                    front_shiny: obj.sprites.front_shiny,
                    back: obj.sprites.back_default,
                    back_shiny: obj.sprites.back_shiny,
                    abilityName: obj.abilities[0].ability.name,
                    moveName: obj.moves[0].move.name,
                    stats: obj.stats,
                    games: obj.game_indices
                });

                fetch(`https://pokeapi.co/api/v2/ability/${obj.abilities[0].ability.name}`,{})
                .then(response => {
                    if(!response.ok) {
                        throw new Error("Not 2xx response", {cause: response})
                    }
                      return response.json()
                    })
                .then(obj => {
                    for(let i=0;i<obj.effect_entries.length;i++){
                        if(obj.effect_entries[i].language.name==="en"){
                            setAbility({ 
                                ability:obj.effect_entries[i].effect});
                        }
                    }
                })

                fetch(`https://pokeapi.co/api/v2/move/${obj.moves[0].move.name}`,{})
                .then(response => {
                    if(!response.ok) {
                        throw new Error("Not 2xx response", {cause: response})
                    }
                      return response.json()
                    })
                .then(obj => {
                    for(let i=0;i<obj.effect_entries.length;i++){
                        if(obj.effect_entries[i].language.name==="en"){
                            setMove({ 
                                move:obj.effect_entries[i].effect});
                        }
                    }
                })
            })
    }

    function getRegion(name){
        setRecentlyClicked({name});
        fetch(`https://pokeapi.co/api/v2/version/${name}`,{}
        )
        .then(response => {
            if(!response.ok) {
                throw new Error("Not 2xx response", {cause: response})
            }
              return response.json()
            })
        .then(obj => {
            fetch(obj.version_group.url,{})
            .then(response => {
                if(!response.ok) {
                    throw new Error("Not 2xx response", {cause: response})
                }
                  return response.json()
                })
            .then(obj=> {
                setRegion({
                    name: obj.regions[0].name})
            })
        })
    }
    
    function selected() {
        let li=document.querySelectorAll("header li")
        li.forEach((e)=>{
            e.className="bold";
        })
    }

    function showMsg(){
        let h3=document.querySelector("div.artist-info div h3");
        if(h3.className=="hide"){
            h3.classList.toggle("hide");
        }
        
    }

    function unHideText(className) {
        let para=document.querySelectorAll("p");
        para.forEach((e)=>{
            if(e.className==className+" hide") {
                e.classList.toggle("hide");
            }else{
                e.classList.add("hide");
            }
        })

        let header=document.querySelectorAll("h4");
        header.forEach((e)=> {
            if(e.className==className+" hide"){
                e.classList.toggle("hide");
            }else{
                e.classList.add("hide");
            }
        })

        let ul=document.querySelectorAll("div.artist-info ul");
        ul.forEach((e)=> {
            if(e.className==className+" hide"){
                e.classList.toggle("hide");
            }else{
                e.classList.add("hide");
            }
        })

        let h3=document.querySelector("div.artist-info div h3");
        h3.classList.add("hide");
    }

    useEffect( () => {
        fetch("https://pokeapi.co/api/v2/type", {})
        .then(response => {
            if(!response.ok) {
                throw new Error("Not 2xx response", {cause: response})
            }
              return response.json();
            })
        .then(obj => {
            setTypes(obj.results);
})

    }, []);

    return (
        <div className="wrapper">
            <LeftSideView types={types} pokemons={pokemons} getDetails={getDetails} getPokemon={getPokemon} selected={selected}/>
            <RightSideView selectedPokemon={selectedPokemon} move={move} ability={ability} unHideText={unHideText} getRegion={getRegion} region={region} recentlyClicked={recentlyClicked} showMsg={showMsg}/>
        </div>
    )
}

ReactDOM.render(
    <App />,
    document.querySelector("#react-root")
);